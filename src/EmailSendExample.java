import com.sendgrid.*;

import java.io.IOException;


public class EmailSendExample {


    public static void main(String[] args) {

        System.out.println("Welcome API");


        final Email from = new Email("adi.rupesh@gmail.com");
        final String subject = "Sending with SendGrid is Fun";
        final Email to = new Email("s.thota@anchorfree.com");

        final Content content = new Content("text/plain", "and easy to do anywhere, even with Java");

        final Mail mail = new Mail(from, subject, to, content);

        SendGrid sg = new SendGrid("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");

        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

}
